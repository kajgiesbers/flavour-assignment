﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="PhoneContact", menuName = "Phone/Contact")]
public class Phone_Contact : ScriptableObject
{
	public string contactName;
	public Sprite contactImg;

}
