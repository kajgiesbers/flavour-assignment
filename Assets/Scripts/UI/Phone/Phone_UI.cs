﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Phone_UI : MonoBehaviour
{
	[SerializeField]
	private List<Phone_Contact> initialPhoneContacts = new List<Phone_Contact>();


	[SerializeField]
	private GameObject phoneContactPrefab = null;

	[SerializeField]
	private Transform phoneContactsHolder;

	[SerializeField]
	private Phone_MessageUI phoneMessengerPrefab= null;

	[SerializeField]
	private Transform phoneMessengerHolder;

	[SerializeField]
	private Dictionary<Phone_Contact, Phone_MessageUI> phoneMessengers = new Dictionary<Phone_Contact, Phone_MessageUI>();


	private void Start()
	{
		initialPhoneContacts.ForEach(x =>
		{
			AddNewContact(x);
		});
	}

	public void RunConversation(Phone_Contact contact, Phone_BaseMessage message)
	{
		if(!phoneMessengers.ContainsKey(contact))
		{
			AddNewContact(contact);
		}
		phoneMessengers[contact].gameObject.SetActive(true);
		StartCoroutine(message.ExecuteMessage(phoneMessengers[contact]));
	}

	private void AddNewContact(Phone_Contact contact)
	{
		phoneMessengers[contact] = Instantiate<Phone_MessageUI>(phoneMessengerPrefab, phoneMessengerHolder);
		phoneMessengers[contact].Setup(this, contact);
		GameObject go = Instantiate(phoneContactPrefab, phoneContactsHolder);
		go.GetComponent<Button>().onClick.AddListener(() => { phoneMessengers[contact].gameObject.SetActive(true); });
		go.GetComponentInChildren<Image>().sprite = contact.contactImg;
	}

	public void ClearAllMessages()
	{
		StopAllCoroutines();
		foreach (KeyValuePair<Phone_Contact, Phone_MessageUI> entry in phoneMessengers)
		{
			entry.Value.ResetTexts();
			entry.Value.ResetOptions();
		}
	}
}
