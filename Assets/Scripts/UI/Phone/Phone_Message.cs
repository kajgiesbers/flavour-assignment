﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

[CreateAssetMenu(fileName ="PhoneMessage", menuName = "Phone/Message")]
public class Phone_Message : Phone_BaseMessage
{
	[SerializeField]
	private float messageDelayInSeconds = 0.5f;

	[SerializeField]
	private List<string> messagesToPlay = new List<string>();


	[SerializeField]
	private float extraDelayBeforeNextMessages = 0.5f;
	[SerializeField]
	private Phone_BaseMessage nextMessage = null;


	public override IEnumerator ExecuteMessage(Phone_MessageUI ui)
	{
		for (int i = 0; i < messagesToPlay.Count; i++)
		{
			yield return new WaitForSeconds(messageDelayInSeconds);
			GameObject go = Instantiate(ui.messagePrefab, ui.messageHolder);
			go.GetComponentInChildren<TextMeshProUGUI>().text = messagesToPlay[i];

			//This is necessary because the Unity ContentSizeFitter doesn't refresh itself properly:
			LayoutRebuilder.ForceRebuildLayoutImmediate(go.GetComponent<RectTransform>());
			go.GetComponent<ContentSizeFitter>().enabled = true;
			ui.ScrollToLatestMessage();
		}

		if(nextMessage != null)
		{
			yield return new WaitForSeconds(extraDelayBeforeNextMessages);
			ui.RunNextMessage(nextMessage);
		}
	}

}
