﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
using UnityEngine.Assertions;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "PhoneOptions", menuName = "Phone/Options")]
public class Phone_Options : Phone_BaseMessage
{
	[SerializeField]
	private List<string> options = new List<string>();

	[SerializeField]
	private List<Phone_BaseMessage> responses = new List<Phone_BaseMessage>();


	public override IEnumerator ExecuteMessage(Phone_MessageUI ui)
	{
		ui.OpenResponseHolder();

		yield return new WaitForSeconds(0.3f);

		//Every option should corresponds to a response
		Assert.IsTrue(options.Count == responses.Count);

		for(int i = 0; i < options.Count; i++)
		{
			GameObject go = Instantiate(ui.responsePrefab, ui.ResponseHolder);
			go.GetComponentInChildren<TextMeshProUGUI>().text = options[i];
			int responseIndex = i;
			go.GetComponent<Button>().onClick.AddListener(() =>
			{
				RunResponse(ui, responseIndex);
			});
			//This is necessary because the Unity ContentSizeFitter doesn't refresh itself properly:
			LayoutRebuilder.ForceRebuildLayoutImmediate(go.GetComponent<RectTransform>());
			go.GetComponent<ContentSizeFitter>().enabled = true;
		}
		yield return null;
	}

	private void RunResponse(Phone_MessageUI ui, int responseIndex)
	{
		//Post the response
		GameObject go = Instantiate(ui.yourMessagePrefab, ui.messageHolder);
		go.GetComponentInChildren<TextMeshProUGUI>().text = options[responseIndex];

		//This is necessary because the Unity ContentSizeFitter doesn't refresh itself properly:
		LayoutRebuilder.ForceRebuildLayoutImmediate(go.GetComponent<RectTransform>());
		LayoutRebuilder.ForceRebuildLayoutImmediate(ui.messageHolder.GetComponent<RectTransform>());
		go.GetComponent<ContentSizeFitter>().enabled = true;

		ui.ResetOptions();

		ui.ScrollToLatestMessage();

		ui.RunNextMessage(responses[responseIndex]);
		ui.CloseResponseHolder();
	}
}
