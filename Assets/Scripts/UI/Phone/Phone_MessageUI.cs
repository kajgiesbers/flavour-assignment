﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Phone_MessageUI : MonoBehaviour
{
	public Transform messageHolder = null;
	public Transform ResponseHolder = null;
	public Transform ResponseParent = null;

	public GameObject messagePrefab = null;
	public GameObject yourMessagePrefab = null;
	public GameObject responsePrefab = null;

	[SerializeField]
	private ScrollRect scrollRect = null;

	[SerializeField]
	private TextMeshProUGUI contactName = null;

	private Phone_UI phoneUI = null;
	private Phone_Contact phoneContact = null;

	public void Setup(Phone_UI parentPhoneUI, Phone_Contact contact)
	{
		contactName.text = contact.contactName;
		phoneUI = parentPhoneUI;
		phoneContact = contact;
	}

	public void RunNextMessage(Phone_BaseMessage nextMessage)
	{
		phoneUI.RunConversation(phoneContact, nextMessage);
	}

	private void OnEnable()
	{
		Canvas.ForceUpdateCanvases();
	}

	public void OpenResponseHolder()
	{
		if(gameObject.activeInHierarchy)
		{
			StartCoroutine(OpenResponseHolderStart());
		}
		else
		{
			ResponseParent.localScale = new Vector3(ResponseParent.localScale.x, 1.0f, ResponseParent.localScale.z);
		}
	}
	public void CloseResponseHolder()
	{
		if (gameObject.activeInHierarchy)
		{
			StartCoroutine(CloseResponseHolderStart());
		}
		else
		{
			ResponseParent.localScale = new Vector3(ResponseParent.localScale.x, 0.0f, ResponseParent.localScale.z);
		}
	}


	private IEnumerator OpenResponseHolderStart()
	{
		const float openTime = 0.3f;
		float startTime = Time.time;

		while (startTime + openTime > Time.time)
		{
			float scaleY = 1.0f - (startTime + openTime - Time.time) / openTime;
			ResponseParent.localScale = new Vector3(ResponseParent.localScale.x, scaleY, ResponseParent.localScale.z);
			yield return null;
		}

		ResponseParent.localScale = new Vector3(ResponseParent.localScale.x, 1.0f, ResponseParent.localScale.z);
	}

	private IEnumerator CloseResponseHolderStart()
	{
		const float closeTime = 0.3f;
		float startTime = Time.time;

		while (startTime + closeTime > Time.time)
		{
			float scaleY = (startTime + closeTime - Time.time) / closeTime;
			ResponseParent.localScale = new Vector3(ResponseParent.localScale.x, scaleY, ResponseParent.localScale.z);
			yield return null;
		}

		ResponseParent.localScale = new Vector3(ResponseParent.localScale.x, 0.0f, ResponseParent.localScale.z);
	}

	public void ResetTexts()
	{
		for (int i = 0; i < messageHolder.childCount; i++)
		{
			Destroy(messageHolder.GetChild(i).gameObject);
		}
	}

	public void ResetOptions()
	{
		for (int i = 0; i < ResponseHolder.childCount; i++)
		{
			Destroy(ResponseHolder.GetChild(i).gameObject);
		}
	}

	public void ScrollToLatestMessage()
	{
		LayoutRebuilder.ForceRebuildLayoutImmediate(scrollRect.GetComponent<RectTransform>());
		scrollRect.normalizedPosition = new Vector2(0, 0);
	}
}
