﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartConversation : MonoBehaviour
{
	[SerializeField]
	private Phone_UI phoneUI = null;

	[SerializeField]
	private Phone_Contact phoneContact = null;

	[SerializeField]
	private Phone_BaseMessage phoneMessageToStart = null;



	public void StartConvo()
	{
		phoneUI.RunConversation(phoneContact, phoneMessageToStart);
	}

}
