﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Phone_BaseMessage : ScriptableObject
{
	public abstract IEnumerator ExecuteMessage(Phone_MessageUI ui);
}
